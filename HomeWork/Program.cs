﻿using System;
using System.Drawing;
using HomeWork.Shape;
using HomeWork.Serialization;

namespace HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            Context context = new Context();
            Carre carre = new Carre(new Point(1,2), new Point(3, 2), new Point(3, 0), new Point(1, 0));
            Console.WriteLine("XML Сериализация объескта класса Carre");
            Console.WriteLine();
            Console.WriteLine(context.Serialize(new StrategySerializationXML(), carre));
            Console.WriteLine();
            Console.WriteLine("JSON Сериализация объескта класса Carre");
            Console.WriteLine();
            Console.WriteLine(context.Serialize(new StrategySerializationJSon(), carre));
            Console.ReadKey();
        }
    }
}
