﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Shape;

namespace HomeWork.Serialization
{
    class Context
    {
        public IStrategy ConcreteStrategy { get; set; }
        public IVisitor ConcreteVisitor { get; set; }
        public void SetStrategy(IStrategy strategy)
        {
            ConcreteStrategy = strategy;
        }
        public void SetVisitor(IVisitor visitor)
        {
            ConcreteVisitor = visitor;
        }
        public string Serialize(IStrategy strategy, IVisitor visitor)
        {
            SetStrategy(strategy);
            SetVisitor(visitor);
            return ConcreteStrategy.Serialize(ConcreteVisitor);
        }
    }
}
