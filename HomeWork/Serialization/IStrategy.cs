﻿using System;
using HomeWork.Shape;

namespace HomeWork.Serialization
{
    interface IStrategy
    {
        string Serialize(IVisitor visitor);
    }
}
