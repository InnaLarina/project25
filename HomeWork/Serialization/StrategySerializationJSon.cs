﻿using System;
using HomeWork.Shape;



namespace HomeWork.Serialization
{
    class StrategySerializationJSon : IStrategy
    {
        public string Serialize(IVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
