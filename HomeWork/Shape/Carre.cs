﻿using HomeWork.Serialization;
using System;
using System.Drawing;

namespace HomeWork.Shape
{
    class Carre : IVisitor
    {
        Point Point1 { get; }
        Point Point2 { get; }
        Point Point3 { get; }
        Point Point4 { get; }
        public Carre(Point point1, Point point2, Point point3, Point point4)
        {
            Point1 = point1;
            Point2 = point2;
            Point3 = point3;
            Point4 = point4;
        }
        public string Visit(StrategySerializationJSon strategy)
        {
            return $"{{\"Point1.X\":{Point1.X}, \"Point1.Y\":{Point1.Y},\"Point2.X\":{Point2.X}, \"Point2.Y\":{Point2.Y},\"Point3.X\":{Point3.X}, \"Point3.Y\":{Point3.Y},\"Point4.X\":{Point4.X}, \"Point4.Y\":{Point4.Y}}}";
        }
        public string Visit(StrategySerializationXML strategy)
        {
            return "< Carre > " + Environment.NewLine +
            "< Point1 >" + Environment.NewLine +
            "  < X > {Point1.X} </ X >" + Environment.NewLine +
            "  < Y > {Point1.Y} </ Y >" + Environment.NewLine +
            "</ Point1 >" + Environment.NewLine +
            "< Point2 >" + Environment.NewLine +
            "  < X > {Point2.X} </ X >" + Environment.NewLine +
            "  < Y > {Point2.Y} </ Y >" + Environment.NewLine +
            "</ Point2 >" + Environment.NewLine +
            "< Point3 >" + Environment.NewLine +
            "  < X > {Point3.X} </ X >" + Environment.NewLine +
            "  < Y > {Point3.Y} </ Y >" + Environment.NewLine +
            "</ Point3 >" + Environment.NewLine +
            "< Point4 >" + Environment.NewLine +
            "  < X > {Point4.X} </ X >" + Environment.NewLine +
            "  < Y > {Point4.Y} </ Y >" + Environment.NewLine +
            "</ Point4 >" + Environment.NewLine +
             "</ Carre >";
        }
    }
}
