﻿using HomeWork.Serialization;
using System;
using System.Drawing;

namespace HomeWork.Shape
{
    class Circle : IVisitor
    {
        public Point Center { get; }
        public int Radius { get; }
        public Circle(Point center, int radius)
        {
            this.Center = center;
            this.Radius = radius;
        }
        public string Visit(StrategySerializationJSon strategy)
        {
            return $"{{\"Center.X\":{Center.X}, \"Center.Y\":{Center.Y}, \"Radius\":{Radius}}}";
        }
        public string Visit(StrategySerializationXML strategy)
        {
            return "< Circle > " + Environment.NewLine +
            "< Center >" + Environment.NewLine +
            $"  < X > {Center.X} </ X >" + Environment.NewLine +
            $"  < Y > {Center.Y} </ Y >" + Environment.NewLine +
            "</ Center >" + Environment.NewLine +
            $"< Radius > {Radius} </ Radius >" + Environment.NewLine +
             "</ Circle >";
        }
    }
}
