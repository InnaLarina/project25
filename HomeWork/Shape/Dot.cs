﻿using HomeWork.Serialization;
using System;
using System.Drawing;




namespace HomeWork.Shape
{
    class Dot : IVisitor
    {
        public Point Center { get; }

        public Dot(Point center)
        {
            Center = center;
        }
        public string Visit(StrategySerializationJSon strategy)
        {
            return $"{{\"Center.X\":{Center.X}, \"Center.Y\":{Center.Y}}}";
        }

        public string Visit(StrategySerializationXML strategy)
        {
            return "< Dot > " + Environment.NewLine +
            "< Center >" + Environment.NewLine +
            String.Format("< X > {0} </ X >", Center.X) + Environment.NewLine +
            String.Format("< Y > {0} </ Y >", Center.Y) + Environment.NewLine +
            "</ Center >" + Environment.NewLine +
             "</ Dot >";
        }
    }
}
