﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Serialization;

namespace HomeWork.Shape
{
    interface IVisitor
    {
        string Visit(StrategySerializationJSon strategy);
        string Visit(StrategySerializationXML strategy);

    }
}
